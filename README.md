[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 5

Problemas propuestos para la Sesión 5 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

Esta es la segunda sesión en el que se exploran las capacidades de las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas. Las tareas definirán clases que implementen la interface [`Callable<V>`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Callable.html) y la interface Runnable para añadirlas al **marco de ejecución**. Los ejercicios son diferentes para cada grupo:

 - [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion5#grupo-1)
 - [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion5#grupo-2)
 - [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion5#grupo-3)
 - [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion5#grupo-4)

## Grupo 1

En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la fabricación de los diferentes componentes de un ordenador que se repetirá cada cierto tiempo, la recolección de esos componentes para instarlos en los diferentes ordenadores que conformarán el pedido, según la disponibilidad, y una última tarea que permitirá la finalización de todas las tareas activas pasado un tiempo establecido. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ScheduledExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html) y [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html) para la ejecución de las diferentes tareas. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Componente`: Clase que representa a un componente de un `TipoComponente` para la construcción de un `Ordenador`.
- `Ordenador`: Representa el ordenador que estará compuesto por un `Componente` de los presentes en `TipoComponente`. En otro caso se considera inacabado.
- `Frabricante`: Simula la fabricación de un componente de ordenador. Para ello será necesario un identificador y un `TipoComponente`.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `TareaFinalizacion`: Su constructor necesita la lista de `Future` que representa las tareas que se deben cancelar y un elemento de sincronización con el hilo principal.
	- Cancela todas las tareas de la lista.
	- Mediante el elemento de sincronización indica al hilo principal que ha completado su tarea.
	
- `CrearComponente`: En el constructor será necesario un `CompletionService` para la ejecución de las tareas de fabricación y una lista de `Future` para almacenar esas tareas que posteriormente pueden ser canceladas:
	- Crea una tarea `Fabricante` con un tipo aleatorio de `TipoComponente` a fabricar.
	- Añade la referencia `Future` a la lista de tareas para su posterior cancelación.
	- Hay que programar un procedimiento de interrupción cuando se solicite desde `TareaFinalizacion`.

- `Proveedor`: Su constructor necesita un `CompletionService` para recoger los componentes fabricados, una lista para almacenar el pedido de ordenadores. El trabajo del proveedor es:
	- Su ejecución se encuentra en un ciclo infinito hasta que se solicite su interrupción para finalizar:
		- Espera a tener un componente fabricado.
		- Recorre la lista de pedidos hasta encontrar un ordenador donde instalar el componente. 
		- Si no es posible se crea un nuevo ordenador al que se le añade el componente. Ese ordenador se añade a la lista de pedidos.

- `Hilo princial`: Realizará los siguientes pasos:
	- Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	- Se crea un `CompletionService` donde se ejecutarán las tareas de `Fabricacion` y se obtienen los `Componente` fabricados. La capacidad que tendrá viene determinada por la constante `FABRICANTES`.
	- Se crea una tarea `CrearComponente` y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `CREAR_COMPONENTE`. La unidad de tiempo es en segundos.
	- Se crea una tarea `Proveedor` y se añade para su ejecución desde el comienzo.
	- Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	- El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	- Cierra los marcos de ejecución.
	- Presenta la lista de pedidos antes de finalizar el hilo principal.

## Grupo 2

En una granja de rendering (render farm) se generan escenas que luego se renderizan. Todo el proceso se realiza concurrentemente. Hay generadores de escenas que recopilan todo lo necesario para renderizar una escena (modelos 3D, texturas, materiales, etc.) y lo ponen a disposición de los renderizadores que construyen las imágenes con la especificación de la escena, mediante una lista de escenas común. (En esta sesión las tareas son simuladas con sleep).

Para la realización de este ejercicio no será necesario crear ninguna clase, pero se deberá terminar la implementación de que se indican más adelante. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor`  que garantice que no se  van a generar nunca mas de `NUM_GENERADORES+NUM_RENDERIZADORES` hilos. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten y `CyclicBarrier` para notificar el final de los Generadores.

Para la solución se tienen que utilizar obligatoriamente los siguientes elementos ya programados:

- `Constantes`: Interface con las constantes y los elementos auxiliares necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Escena`: Representa a una escena que se tiene que renderizar. Tiene asociado un tiempo de renderización que se genera aleatoriamente al construir la escena.
- `Finalizador`: Es la clase encargada de notificar a los Renderizadores de escenas que los generadores han terminado.

En el programa se tienen las siguientes tareas:

-   `GeneradorEscena`
    -   Tiene acceso a la lista de escenas pendientes.
    -   Genera Escenas y las añade a la lista.
        -   La generación de una escena consume un tiempo aleatorio de entre `MIN_TIEMPO_GENERACION` y `MIN_TIEMPO_GENERACION+VARIACION_GENERACION` segundos.
        -   La tarea de generar escenas termina cuando se hayan generado un número aleatorio de escenas comprendido entre `MIN_ESCENAS_X_GENERADOR` y `MIN_ESCENAS_X_GENERADOR + VARIACION_ESCENAS_X_GENERADOR`.
        -   Cuando se completa la tarea se devuelve una lista con las escenas que se han generado.
-   `RenderizadorEscena`
    -   Tiene acceso a la lista de escenas pendientes.
    -   Selecciona la siguiente escena disponible y la renderiza.
        -   La renderización de una escena consume un tiempo de entre `MIN_DURACION_ESCENA` y `MIN_DURACION_ESCENA + VARIACION_DURACION` que se genera aleatoriamente  **al construir la escena.**
        -   La tarea de renderizar escenas finaliza cuando todas las tareas GenerarEscenas han terminado y ya no quedan mas escenas en la lista de escenas pendientes.
        -   Cuando se completa la tarea se devuelve una lista con las escenas que se han procesado.
-   `Hilo Principal`
    -   Crea la lista de escenas pendientes.
    -   Crea e inicia la ejecución de `NUM_GENERADORES` Tareas GeneradorEscenas.
    -   Crea e inicia la ejecución de `NUM_RENDERIZADORES` Tareas RenderizadorEscenas.
    -   Espera a que todas las tareas GeneradorEscenas hayan terminado y entonces notifica a las tareas RenderizadorEscenas que deben continuar hasta que la lista de escenas pendientes se quede vacía. En ese momento las tareas RenderizadorEscenas deben terminar.
    -   Esperará hasta que todas las tareas estén completas.
    -   Muestra para cada Tarea Generador de escenas y cada Tarea Renderizador de escenas las escenas que ha generado / renderizado.
    -   Muestra las escenas que hayan quedado sin renderizar en la lista de escenas pendientes.

## Grupo 3

En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la planificación de los diferentes procesos que se ejecutan en el procesador de un ordenador que se repetirá cada cierto tiempo, la recopilación de informes de ejecución de los procesos que han finalizado, periódicamente, y una última tarea que permitirá la finalización de todas las tareas activas pasado un tiempo establecido. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ScheduledExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html) y [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html) para la ejecución de las diferentes tareas. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Proceso`: Representa al proceso que simulará su ejecución en una unidad de procesamiento.
- `HiloEjecucion`: Esta tarea simula la ejecución de un `TipoProceso` en un hilo del sistema. A su finalización nos devolverá el `Proceso` que se ha ejecutado.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan:

- `TareaFinalizacion`: Su constructor necesita la lista de `Future` que representa las tareas que se deben cancelar y un elemento de sincronización con el hilo principal.
	- Cancela todas las tareas de la lista.
	- Mediante el elemento de sincronización indica al hilo principal que ha completado su tarea.

- `Planificador`: Su constructor necesita el `CompletionService` donde se simulará la ejecución de los procesos y la lista de `Future` donde añadirá las tareas que ha añadido al **marco de ejecución**. En su ejecución realizará las siguientes tareas, si no se solicita su interrupción:
	- Crea un `HiloEjecucion` asociándole un `TipoProceso` aleatorio.
	- Se añade para su ejecución al `CompletionService` y se almacena el `Future` resultante en la lista de tareas que pueden ser canceladas.

- `InformeProcesador`: Su constructor necesita el `CompletionService` para obtener los procesos que han finalizado su ejecución. Cada vez que se ejecute, si no se ha pedido su cancelación, deberá:
	- Obtener todos los `Proceso` que haya en el `CompletionService`.
	- Mostrar la lista en la consola indicando el número de informe, es decir, los informes tienen una numeración creciente durante la ejecución de la tarea.

- `Hilo princial`: Realizará los siguientes pasos:
	- Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	- Se crea un `CompletionService` donde se ejecutarán las tareas de `HiloEjecucion` y se obtienen los `Proceso` completados. La capacidad que tendrá viene determinada por la constante `NUCEOS`.
	- Se crea una tarea `Planificador` y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `PLANIFICADOR`. La unidad de tiempo es en segundos.
	- Se crea una tarea `InformeProcesador` y se añade para su ejecución con un comienzo retardado de `INICIO_INFORME` y con un periodo de repetición de `INFORME`. La unidad de tiempo es en segundos.
	- Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	- El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	- Cierra los marcos de ejecución.
	- Finaliza el hilo principal.

## Grupo 4

En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la solicitud de instalación de un número de sensores en una casa que se repetirá cada cierto tiempo, la recopilación de las casas en las que se ha finalizado la instalación, esperando un tiempo acordado y de forma periódica en etapas, y una última tarea que permitirá la finalización de todas las tareas activas pasado un tiempo establecido. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ScheduledExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html) y [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html) para la ejecución de las diferentes tareas. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Sensor`: Simula un tipo de sensor y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Casa`: Simula la casa donde se instalará un sensor. Dispone de una distribución de habitaciones según el `TipoCasa` que viene definido en `Constantes`.
- `Instalador`: Simula la instalación de un número de sensores aleatorios en una casa. A la finalización de su trabajo se obtiene la casa donde se instalaron los sensores.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `TareaFinalizacion`: Su constructor necesita la lista de `Future` que representa las tareas que se deben cancelar y un elemento de sincronización con el hilo principal.
	- Cancela todas las tareas de la lista.
	- Mediante el elemento de sincronización indica al hilo principal que ha completado su tarea.

- `InstalarCasa`: Su constructor necesita el `CompletionService` donde se ejecutarán las tareas de instalación y la lista de `Future` donde se añadirá la tarea de instalación que se ha ordenado ejecutar para una cancelación posterior si fuera necesario. Las acciones que tiene que realizar, si no se ha solicitado su interrupción, son:
	- Generar un número aleatorio de sensores con un mínimo de `NUM_SENSORES` y una variación aleatoria expresada por `VARIACION`.
	- Añade la tarea `Instalador` a marco de ejecución y almacena en la lista de `Future` el resultado de esa acción.

- `Promotor`: Su constructor necesita el `CompletionService` desde donde obtendrá la `Casa` en la que se han instalado los sensores y una lista para añadir las diferentes listas de casas de cada una de las etapas que complete el promotor. 
	- Cada ejecución del `Promotor` es una etapa y deberá recoger las casas que hayan finalizado hasta ese momento.
	- El `Promotor` espera por un tiempo, expresado por la constante `ESPERA_INSTALADOR`, cada casa que debe añadir a la lista de la etapa. Si se supera ese tiempo ha finalizado la etapa y ya no se añaden más casas.
	- Solo se añadirá la lista con las casas de una etapa a la lista total si se ha podido completar completamente la recogida de información de esa etapa.

- `Hilo princial`: Realizará los siguientes pasos:
	- Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	- Se crea un `CompletionService` donde se ejecutarán las tareas de `Instalador` y se obtienen las `Casa` completadas con su instalación de sensores. La capacidad que tendrá viene determinada por la constante `INSTALADORES`.
	- Se crea una tarea `InstalarCasa` y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `INSTALACION`. La unidad de tiempo es en segundos.
	- Se crea una tarea `Promotor` y se añade para su ejecución con un comienzo retardado de `INICIO_PROMOTOR` y con un periodo de repetición de `ESPERA_INSTALADOR`. La unidad de tiempo es en segundos.
	- Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	- El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	- Cierra los marcos de ejecución.
	- Antes de finalizar se muestra la lista con la lista de las casas en cada etapa. Debe quedar claro las casas que pertenecen a cada etapa en la salida.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTgyMjUyOTYxMCwtMjA4NDM1OTM0OCwxMD
AyMjc5NThdfQ==
-->