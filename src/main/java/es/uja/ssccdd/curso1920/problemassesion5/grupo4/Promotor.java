/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.ESPERA_INSTALADOR;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Promotor implements Runnable {
    private final CompletionService<Casa> instaladores;
    private final List<List<Casa>> listaCasas;
    private int numEtapa;

    public Promotor(CompletionService<Casa> instaladores, List<List<Casa>> listaCasas) {
        this.instaladores = instaladores;
        this.listaCasas = listaCasas;
        this.numEtapa = 1;
    }

    
    @Override
    public void run() {
        System.out.println("TAREA-Promotor Recoge las casas de la " + numEtapa + " ª ETAPA");
        
        try {
            presentarEtapa();
        } catch (InterruptedException ex) {
            System.out.println("TAREA-Promotor se CANCELA la ETAPA");
        } catch (ExecutionException ex) {
            System.out.println("TAREA-Promotor ERROR en la instalación");
        }
    }
    
    private void presentarEtapa() throws InterruptedException, ExecutionException {
        Future<Casa> casa;
        ArrayList<Casa> casasEtapa = new ArrayList();
        
        // Se muestran todos los procesos que han terminado hasta el momento
        while( (casa = instaladores.poll(ESPERA_INSTALADOR, TimeUnit.SECONDS)) != null ) 
            casasEtapa.add(casa.get());
        
        listaCasas.add(casasEtapa);
        numEtapa++;
    }
}
