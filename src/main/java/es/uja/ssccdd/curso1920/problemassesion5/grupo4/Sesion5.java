/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion5.grupo1.Constantes.ESPERA_FINALIZACION;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo1.Constantes.SISTEMA;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.ESPERA_INSTALADOR;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.INICIO_PROMOTOR;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.INSTALACION;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.INSTALADORES;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucionSistema;
        ExecutorService ejecucion;
        CompletionService<Casa> instaladores;
        CountDownLatch esperaFinalizacion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        List<List<Casa>> listaCasas;
     
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucionSistema = Executors.newScheduledThreadPool(SISTEMA);
        ejecucion = Executors.newFixedThreadPool(INSTALADORES);
        instaladores = new ExecutorCompletionService(ejecucion);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        listaTareas = new ArrayList();
        listaCasas = new ArrayList();
        
        // Se añade la tarea ordenInstalacion que se ejecutará periódicamente
        InstalarCasa ordenInstalacion = new InstalarCasa(instaladores,listaTareas);
        tarea = ejecucionSistema.scheduleAtFixedRate(ordenInstalacion, INICIO, 
                                                        INSTALACION, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se añade la tarea del promotor que presenta las casas de cada etapa
        Promotor promotor = new Promotor(instaladores, listaCasas);
        tarea = ejecucionSistema.scheduleAtFixedRate(promotor, INICIO_PROMOTOR, 
                                                        ESPERA_INSTALADOR, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdown();
        ejecucion.shutdown();
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) el pedido obtenido es \n______________");
        for( int i = 0; i < listaCasas.size(); i++ ) {
            System.out.println("________ ETAPA " + (i+1) + " ________");
            for( Casa casa : listaCasas.get(i) )
                System.out.println(casa.toString());
            System.out.println("________ FIN ETAPA ________");
        }
            
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
