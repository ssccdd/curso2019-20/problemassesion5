/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion5.grupo1.Constantes.ESPERA_FINALIZACION;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo1.Constantes.SISTEMA;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.INFORME;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.INICIO_INFORME;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.NUCLEOS;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.PLANIFICADOR;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.TIEMPO_ESPERA;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService ejecucionSistema;
        ExecutorService ejecucion;
        CompletionService<Proceso> nucleos;
        CountDownLatch esperaFinalizacion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
     
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        ejecucionSistema = Executors.newScheduledThreadPool(SISTEMA);
        ejecucion = Executors.newFixedThreadPool(NUCLEOS);
        nucleos = new ExecutorCompletionService(ejecucion);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        listaTareas = new ArrayList();
        
        // Se añade el planificador que se ejecutará periódicamente
        Planificador planificador = new Planificador(nucleos,listaTareas);
        tarea = ejecucionSistema.scheduleAtFixedRate(planificador, INICIO, PLANIFICADOR, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se añade la tarea que genera los informes de ejecución de los procesos
        InformeProcesador informe = new InformeProcesador(nucleos);
        tarea = ejecucionSistema.scheduleAtFixedRate(informe, INICIO_INFORME, INFORME, TimeUnit.SECONDS);
        listaTareas.add(tarea); // Se añade para la cancelación
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdown();
        ejecucion.shutdown();
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
