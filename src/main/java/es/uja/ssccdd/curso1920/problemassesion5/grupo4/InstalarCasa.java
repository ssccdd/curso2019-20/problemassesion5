/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.NUM_SENSORES;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion5.grupo4.Constantes.aleatorio;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class InstalarCasa implements Runnable {
    private final CompletionService<Casa> instaladores;
    private final List<Future<?>> listaTareas;
    private int numCasa;

    public InstalarCasa(CompletionService<Casa> instaladores, List<Future<?>> listaTareas) {
        this.instaladores = instaladores;
        this.listaTareas = listaTareas;
        this.numCasa = 1;
    }

    
    @Override
    public void run() {
        System.out.println("TAREA-InstalarCasa se instalan los sensores en " 
                                    + numCasa + "º casa");
        
        try {
            instalarCasa();
        } catch (InterruptedException ex) {
            System.out.println("TAREA-InstalarCasa CANCELA la instalación de la " 
                                    + numCasa + "º casa");
        }
        
    }
    
    private void instalarCasa() throws InterruptedException {
        Future<Casa> tarea;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        int numSensores = aleatorio.nextInt(VARIACION) + NUM_SENSORES;
        Instalador instalador = new Instalador("Instalación("+numCasa+")", numSensores);
        numCasa++;
        
        tarea = instaladores.submit(instalador);
        listaTareas.add(tarea);
    }
}
