/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que simula la renderizacion de escenas.
 * @author fconde
 */
public class RenderizadorEscenas implements Callable<List<Escena>> {

    // - Variable de clase. Es compartida por todos los Renderizadores y se usa
    //   para notificar que los generadores han terminado.
    private static boolean terminar = false;
    
    private final String id;
    private final ArrayList<Escena> arrayEscenas;
    private final ReentrantLock lock;

    public static void setTerminar(boolean terminar) {
        RenderizadorEscenas.terminar = terminar;
    }

    public RenderizadorEscenas(String id, ArrayList<Escena> arrayEscenas, ReentrantLock lock) {
        this.id = id;
        this.arrayEscenas = arrayEscenas;
        this.lock = lock;
    }   
    
    @Override
    public List<Escena> call() {
        System.out.println("Ejecutando "+id);
        ArrayList<Escena> resultado = new ArrayList<Escena>();
        Escena escena = null;
        boolean tengoEscena;
        while (true) {
            tengoEscena = false;
            lock.lock();
            try {
                if (!arrayEscenas.isEmpty()) {
                    escena = arrayEscenas.remove(0);
                    resultado.add(escena);
                    tengoEscena = true;
                } else if(terminar) {
                    System.out.println("Terminando ejecución "+id);
                    return resultado;
                }
            } finally {
                lock.unlock();
            }
            
            if (tengoEscena) {
                try {
                    TimeUnit.SECONDS.sleep(escena.getDuracion());
                } catch (InterruptedException ex) {
                    Logger.getLogger(RenderizadorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
