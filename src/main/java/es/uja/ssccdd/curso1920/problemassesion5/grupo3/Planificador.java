/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo3;

import es.uja.ssccdd.curso1920.problemassesion5.grupo3.Constantes.TipoProceso;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class Planificador implements Runnable {
    private final CompletionService<Proceso> nucleos;
    private final List<Future<?>> listaTareas;
    private int pID;

    public Planificador(CompletionService<Proceso> nucleos, List<Future<?>> listaTareas) {
        this.nucleos = nucleos;
        this.listaTareas = listaTareas;
        this.pID = 1;
    }
    
    

    @Override
    public void run() {
        System.out.println("TAREA-Planificador ejecuta el proceso " + pID);
        
        try {
            ejecucion();
        } catch (InterruptedException ex) {
            System.out.println("TAREA-Planificador ha sido CANCELADO");
        }
    }
    
    private void ejecucion() throws InterruptedException {
        Future<Proceso> tarea;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        HiloEjecucion hilo = new HiloEjecucion("HiloEjecucion("+pID+")", 
                                                TipoProceso.getProceso());
        pID++;
        
        tarea = nucleos.submit(hilo);
        listaTareas.add(tarea);
    }
}
