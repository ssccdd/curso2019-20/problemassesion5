/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo2;

/**
 * Tarea encargada de avisar a los Renderiadores de que los Generadores ya han
 * terminado.
 * @author fconde
 */
public class Finalizador implements Runnable {
    
    @Override
    public void run() {
        System.out.println("\n-- FINALIZADOR: Todos los generadores han terminado");
        System.out.println("                Notificando a los renderizadores para que terminen\n");
        RenderizadorEscenas.setTerminar(true);
    }
    
}
