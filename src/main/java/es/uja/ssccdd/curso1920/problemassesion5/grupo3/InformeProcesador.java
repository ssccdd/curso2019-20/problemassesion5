/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo3;

import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class InformeProcesador implements Runnable {
    private final CompletionService<Proceso> nucleos;
    private int numInforme;

    public InformeProcesador(CompletionService<Proceso> nucleos) {
        this.nucleos = nucleos;
        this.numInforme = 1;
    }
    
    @Override
    public void run() {
        System.out.println("TAREA-InformeProcesador "
                    + "________ INFORME " + numInforme + " ________");
        
        try {
            generarInforme();
            
            System.out.println("TAREA-InformeProcesador ________ Finalizado ________");
        } catch (InterruptedException ex) {
            System.out.println("TAREA-InformeProcesador ________ CANCELADO ________");
        } catch (ExecutionException ex) {
            System.out.println("TAREA-InformeProcesador ERROR en la ejecución de los procesos");
        }
        
    }
    
    private void generarInforme() throws InterruptedException, ExecutionException {
        Future<Proceso> proceso;
        
        // Se muestran todos los procesos que han terminado hasta el momento
        while( (proceso = nucleos.poll()) != null ) 
            System.out.println(proceso.get().toString());
        
        numInforme++;
    }
}
