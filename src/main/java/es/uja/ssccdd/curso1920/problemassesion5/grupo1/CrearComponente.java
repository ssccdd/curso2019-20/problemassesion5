/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo1;

import es.uja.ssccdd.curso1920.problemassesion5.grupo1.Constantes.TipoComponente;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class CrearComponente implements Runnable {
    private final CompletionService<Componente> fabricacion;
    private final List<Future<?>> listaTareas;
    private int numSerie;

    public CrearComponente(CompletionService<Componente> fabricacion, List<Future<?>> listaTareas) {
        this.fabricacion = fabricacion;
        this.listaTareas = listaTareas;
        this.numSerie = 1;
    }

    @Override
    public void run() {
        System.out.println("TAREA-CrearComonente se fabrica la " + numSerie + "º pieza");
        
        try {
            
            fabricar();
            
        } catch ( InterruptedException ex ) {
            System.out.println("TAREA-CrearComonente CANCELA la fabricación");
        }
            
    }
    
    private void fabricar() throws InterruptedException {
        Future<Componente> tarea;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        Fabricante fabricante = new Fabricante("Fabricante("+numSerie+")", 
                                                TipoComponente.getComponente());
        numSerie++;
        
        tarea = fabricacion.submit(fabricante);
        listaTareas.add(tarea);
    }
}
