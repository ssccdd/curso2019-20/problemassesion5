/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion5.grupo1;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Runnable {
    private final CompletionService<Componente> fabricacion;
    private final List<Ordenador> pedido;
    private int numSerie;

    public Proveedor(CompletionService<Componente> fabricacion, List<Ordenador> pedido) {
        this.numSerie = 1;
        this.fabricacion = fabricacion;
        this.pedido = pedido;
    }
    
    @Override
    public void run() {
        System.out.println("TAREA-Proveedor empieza la preparación del pedido de ordenadores");
        
        try {
            
            while( true )
                prepararPedido();
            
        } catch ( InterruptedException ex ) {
            System.out.println("TAREA-Proveedor finaliza el pedido de ordenadores");
        } catch (ExecutionException ex) {
            System.out.println("TAREA-Proveedor ERROR en la fabricación");
        }
    }
    
    private void prepararPedido() throws InterruptedException, ExecutionException {
        Future<Componente> fabricado;
        Componente componente;
        Ordenador ordenador;
        Iterator it = pedido.iterator();
        boolean asignado = false;
        
        // Espera hasta tener un componente
        fabricado = fabricacion.take();
        componente = fabricado.get();
        
        // Asigna componente a un ordenador si es posible
        while ( it.hasNext() && !asignado ) {
            ordenador = (Ordenador) it.next();
            asignado = ordenador.addComponente(componente);
        }
        
        // Si no se ha podido asignar el componente se crea un nuevo ordenador
        // al pedido y se le asigna el componente
        if( !asignado ) {
            numSerie++;
            ordenador = new Ordenador();
            ordenador.setiD("Proveedor("+numSerie+")");
            ordenador.addComponente(componente);
            pedido.add(ordenador);
        }
    }
}
